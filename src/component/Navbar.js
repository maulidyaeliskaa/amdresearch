import React, { useState } from "react";
import Notifikasi from "./Notifikasi";
import Profile from "./Profile";

export default function Navbar({ profileDetail }) {
  const [isProfileVisible, setIsProfileVisible] = useState(false);
  const [isNotifikasiVisible, setIsNotifikasiVisible] = useState(false);

  function profile() {
    setIsProfileVisible(!isProfileVisible);
  }

  function notifikasi() {
    setIsNotifikasiVisible(!isNotifikasiVisible);
  }

  return (
    <div id="Navbar">
      <div id="XbILl5df2E">
        <div id="tU8YcNlVg5">
          <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M5 11C5 7.691 7.691 5 11 5C14.309 5 17 7.691 17 11C17 14.309 14.309 17 11 17C7.691 17 5 14.309 5 11ZM20.707 19.293L17.312 15.897C18.365 14.543 19 12.846 19 11C19 6.589 15.411 3 11 3C6.589 3 3 6.589 3 11C3 15.411 6.589 19 11 19C12.846 19 14.543 18.365 15.897 17.312L19.293 20.707C19.488 20.902 19.744 21 20 21C20.256 21 20.512 20.902 20.707 20.707C21.098 20.316 21.098 19.684 20.707 19.293Z"
              fill="black"
            />
          </svg>
          <input type="text" placeholder="Search"></input>
        </div>
        <div id="GJy5jPhNjO">
          <div id="RSJaYflnO6">
            <p className="cursor-default">{profileDetail?.nama}</p>
            <p className="cursor-default">{profileDetail?.role_name}</p>
          </div>
          <div>
            <div
              id="Profile"
              style={{ backgroundImage: "url(/assets/images/Photo.jpg)" }}
              className="cursor-pointer"
              onClick={profile}
            ></div>
            {isProfileVisible && (
              <Profile setIsProfileVisible={setIsProfileVisible} />
            )}
          </div>
          <svg
            width="30"
            height="34"
            viewBox="0 0 30 34"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            onClick={notifikasi}
            className="cursor-pointer"
          >
            <path
              d="M20.1154 27.9167C20.1154 30.54 17.9873 32.6667 15.3621 32.6667C12.737 32.6667 10.6088 30.54 10.6088 27.9167M18.2086 7.71105C18.8986 6.99868 19.3232 6.02806 19.3232 4.95833C19.3232 2.77221 17.5498 1 15.3621 1C13.1745 1 11.4011 2.77221 11.4011 4.95833C11.4011 6.02806 11.8257 6.99868 12.5157 7.71105M24.8687 15.5667C24.8687 13.3831 23.8671 11.2889 22.0843 9.74482C20.3015 8.20077 17.8834 7.33333 15.3621 7.33333C12.8408 7.33333 10.4228 8.20077 8.63996 9.74482C6.85712 11.2889 5.85554 13.3831 5.85554 15.5667C5.85554 19.1795 4.95896 21.8218 3.84024 23.7124C2.56521 25.8672 1.92769 26.9446 1.95287 27.202C1.98167 27.4965 2.0347 27.5894 2.27367 27.764C2.48255 27.9167 3.53198 27.9167 5.63085 27.9167H25.0934C27.1923 27.9167 28.2417 27.9167 28.4506 27.764C28.6896 27.5894 28.7426 27.4965 28.7714 27.202C28.7966 26.9446 28.1591 25.8672 26.884 23.7124C25.7653 21.8218 24.8687 19.1795 24.8687 15.5667Z"
              stroke="#28292E"
              strokeWidth="2"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        </div>
      </div>
      {isNotifikasiVisible && (
        <Notifikasi isNotifikasiVisible={isNotifikasiVisible} />
      )}
    </div>
  );
}
