export default function ResearchPhaseTop(){
    return(
      <div id='ResearchPhaseTop'>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Define</p>
          <div id="ResearchPhaseTopCards">
            <p>6</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Design</p>
          <div id="ResearchPhaseTopCards">
            <p>3</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Develop</p>
          <div id="ResearchPhaseTopCards">
            <p>1</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Deploy</p>
          <div id="ResearchPhaseTopCards">
            <p>0</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Delivery</p>
          <div id="ResearchPhaseTopCards">
            <p>0</p>
            <p>Research</p>
          </div>
        </div>
      </div>
    )
}