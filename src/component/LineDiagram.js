import { Line } from "react-chartjs-2";
import {
    Chart as ChartJS,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend
} from 'chart.js';

ChartJS.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend
)

export default function LineDiagram(){

    const pointImageRed = new Image();
    pointImageRed.src = '/assets/pointStyle/red.svg';
    const pointImageOrange = new Image();
    pointImageOrange.src = '/assets/pointStyle/orange.svg';
    const pointImageGreen = new Image();
    pointImageGreen.src = '/assets/pointStyle/green.svg';
    const pointImageBlue = new Image();
    pointImageBlue.src = '/assets/pointStyle/blue.svg';

    const data = {
        labels: ['Mon', 'Tue', 'Wed'],
        datasets: [
            {
            label: 'Restless',
            data: [0, 1, 2],
            backgroundColor: 'transparent',
            borderColor: 'red',
            pointBorderColor: 'red',
            pointRadius: 0,
            fill: true,
            tension: 0.4,
            pointStyle: [pointImageRed, '', '', pointImageRed]
        },
        {
            label: 'Awake',
            data: [0, 4, 3],
            backgroundColor: 'transparent',
            borderColor: 'orange',
            pointBorderColor: 'orange',
            pointRadius: 0,
            fill: true,
            tension: 0.4,
            pointStyle: [pointImageOrange, '', '', pointImageOrange]
        },
        {
            label: 'Deep',
            data: [0, 2, 4],
            backgroundColor: 'transparent',
            borderColor: 'green',
            pointBorderColor: 'green',
            pointRadius: 0,
            fill: true,
            tension: 0.4,
            pointStyle: [pointImageGreen, '', '', pointImageGreen]
        },
        {
            label: 'Time',
            data: [0, 6, 5],
            backgroundColor: 'transparent',
            borderColor: 'blue',
            pointBorderColor: 'blue',
            pointRadius: 0,
            fill: true,
            tension: 0.4,
            pointStyle: [pointImageBlue, '', '', pointImageBlue]
        }
    ]
    }

    const options = {
        plugins: {
            title: {
                display: true,
                text: 'Line Diagram',
            },
            legend: {
                labels: {
                    usePointStyle: true,
                }
            }
        },
        scales: {
            y: {
                display: false,
                min: 0,
                max: 12
            },
            x:{
                display: false
            }
        }
    }

    return(
        <div id="LineDiagram">
            <p>Line Diagram</p>
            <Line
                data={data}
                options={options}
            ></Line>
        
        </div>
    )
}